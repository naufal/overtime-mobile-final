package docotel.laporanta.Handlers;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by VanZa on 8/22/2016.
 */
public class RequestHandler {
    private RequestQueue Queue;

    private RequestQueue getRequestQueue(Context context) {
        if (Queue == null) {
            Queue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return Queue;
    }

    public <T> void addToRequestQueue(Context context, Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(2000, 10, 0));
        getRequestQueue(context).add(req);
    }
}
