package docotel.laporanta.Handlers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

import docotel.laporanta.Listeners.GetListListener;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public abstract class GetListHandler {
    private GetListListener Listener;

    public GetListHandler(final GetListListener Listener) {
        this.Listener = Listener;
    }

    public void GetList(@Nullable Integer Page) {}

    public void GetList(@NonNull Context context,
                        @NonNull int Method,
                        @NonNull final String URL,
                        final Map<String,String> Params,
                        final Map<String,String> Headers,
                        @NonNull Response.Listener<String> onResponse,
                        @NonNull Response.ErrorListener errorListener) {
        StringRequest GetListRequest = new StringRequest(Method, URL, onResponse, errorListener) {
            @Override
            public Map<String,String> getParams() {
                return Params;
            }
            @Override
            public Map<String,String> getHeaders() {
                return Headers;
            }
        };
        new RequestHandler().addToRequestQueue(context, GetListRequest);
    }
}
