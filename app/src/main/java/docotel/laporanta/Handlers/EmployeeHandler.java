package docotel.laporanta.Handlers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.Models.EmployeeModel;

/**
 * Created by VanZa on 8/23/2016.
 */
public class EmployeeHandler extends GetListHandler {
    GetListListener Listener;
    Context context;
    int Method;
    String URL = "";
    Map<String,String> Params = new HashMap<>();
    Map<String,String> Headers = new HashMap<>();

    public EmployeeHandler(@NonNull final GetListListener Listener,
                           @NonNull Context context,
                           @NonNull int Method,
                           @NonNull final String URL,
                           Map<String,String> Params,
                           Map<String,String> Headers) {
        super(Listener);
        this.Listener = Listener;
        this.context = context;
        this.Method = Method;
        this.URL = URL;
        this.Params = Params;
        this.Headers = Headers;
    }

    @Override
    public void GetList(@Nullable Integer Page) {
        if (Headers == null) Headers = new HashMap<>();
        Headers.put("Authorization", "Bearer " + ApplicationHelper.getToken(context));
        String Endpoint = "";
        if (Page != null) Endpoint = URL + "?page=" + Page;
        else Endpoint = URL;
        super.GetList(context, Method, Endpoint, Params, Headers,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject Result = new JSONObject(response);
                            JSONArray JSONEmployees = Result.getJSONArray("data");
                            List<EmployeeModel> EmployeeList = new ArrayList<>();
                            for (int i = 0; i < JSONEmployees.length(); i++) {
                                JSONObject Employee = JSONEmployees.getJSONObject(i);
                                EmployeeList.add(new EmployeeModel(
                                        Employee.getInt("id"),
                                        Employee.getString("position"),
                                        Employee.getString("name")
                                ));
                            }
                            int TotalPage = Result.getInt("last_page");
                            Listener.onSuccess(EmployeeList, TotalPage);
                        } catch (JSONException e) {
                            Listener.onFail(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFail(error.getLocalizedMessage());
                    }
                }
        );
    }
}
//    private EmployeeListener Listener;
//
//    public EmployeeHandler(EmployeeListener listener){
//        this.Listener = listener;
//    }
//
//    public void GetEmployees (Context context, final String token, final int page){
//        StringRequest GetEmployeesRequest = new StringRequest(Request.Method.GET,
//                APIHandler.getEmployeesURL() + "?page=" + String.valueOf(page),
//                new Response.Listener<String>(){
//                    @Override
//                    public void onResponse(String response){

//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Listener.onFail(error.getMessage());
//                    }
//                }) {
////            public Map<String,String> getParams() {
////                Map<String,String> Parameters = new HashMap<>();
////                Parameters.put("page", String.valueOf(page));
////                return Parameters;
////            }
//            public Map<String,String> getHeaders() {
//                Map<String,String> Headers = new HashMap<>();
//                Headers.put("Authorization", "Bearer " + token);
//                return Headers;
//            }
//        };
//        RequestHandler.getInstance(context).addToRequestQueue(GetEmployeesRequest);
//    }
//}