package docotel.laporanta.Handlers;
import android.content.Context;
import docotel.laporanta.R;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class ApplicationHelper {
    private static ApplicationHelper ourInstance = new ApplicationHelper();

    public static ApplicationHelper getInstance() {
        return ourInstance;
    }

    private ApplicationHelper() {
    }

    public static String getToken(Context context) {
        return SharedPreferencesHelper.getStringFromSharedPreferences(context,
                context.getString(R.string.SharedPreferenceName),
                Context.MODE_PRIVATE,
                context.getString(R.string.SharedPreferenceKey_Token),
                "");
    }

    public static void setToken(Context context, String token) {
        SharedPreferencesHelper.putStringToSharedPreferences(context,
                context.getString(R.string.SharedPreferenceName),
                Context.MODE_PRIVATE,
                context.getString(R.string.SharedPreferenceKey_Token),
                token);
    }
}
