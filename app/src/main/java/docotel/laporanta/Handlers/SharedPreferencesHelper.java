package docotel.laporanta.Handlers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class SharedPreferencesHelper {
    private static SharedPreferencesHelper ourInstance = new SharedPreferencesHelper();

    public static SharedPreferencesHelper getInstance() {
        return ourInstance;
    }

    private SharedPreferencesHelper() {
    }

    public static int getIntFromSharedPreferences(Context context,
                                                  String PreferenceName,
                                                  int Mode,
                                                  String PreferenceKey,
                                                  int DefaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        return sharedPreferences.getInt(PreferenceKey, DefaultValue);
    }

    public static String getStringFromSharedPreferences(Context context,
                                                        String PreferenceName,
                                                        int Mode,
                                                        String PreferenceKey,
                                                        String DefaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        return sharedPreferences.getString(PreferenceKey, DefaultValue);
    }

    public static boolean getBooleanFromSharedPreferences(Context context,
                                                          String PreferenceName,
                                                          int Mode,
                                                          String PreferenceKey,
                                                          boolean DefaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        return sharedPreferences.getBoolean(PreferenceKey, DefaultValue);
    }

    public static float getFloatFromSharedPreferences(Context context,
                                                      String PreferenceName,
                                                      int Mode,
                                                      String PreferenceKey,
                                                      float DefaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        return sharedPreferences.getFloat(PreferenceKey, DefaultValue);
    }

    public static long getLongFromSharedPreferences(Context context,
                                                    String PreferenceName,
                                                    int Mode,
                                                    String PreferenceKey,
                                                    long DefaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        return sharedPreferences.getLong(PreferenceKey, DefaultValue);
    }

    public static void putIntToSharedPreferences(Context context,
                                                 String PreferenceName,
                                                 int Mode,
                                                 String PreferenceKey,
                                                 int Value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(PreferenceKey, Value);
        editor.commit();
    }

    public static void putStringToSharedPreferences(Context context,
                                                    String PreferenceName,
                                                    int Mode,
                                                    String PreferenceKey,
                                                    String Value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PreferenceKey, Value);
        editor.commit();
    }

    public static void putBooleanToSharedPreferences(Context context,
                                                     String PreferenceName,
                                                     int Mode,
                                                     String PreferenceKey,
                                                     boolean Value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(PreferenceKey, Value);
        editor.commit();
    }

    public static void putFloatToSharedPreferences(Context context,
                                                   String PreferenceName,
                                                   int Mode,
                                                   String PreferenceKey,
                                                   float Value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(PreferenceKey, Value);
        editor.commit();
    }

    public static void putLongToSharedPreferences(Context context,
                                                  String PreferenceName,
                                                  int Mode,
                                                  String PreferenceKey,
                                                  long Value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceName, Mode);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(PreferenceKey, Value);
        editor.commit();
    }
}
