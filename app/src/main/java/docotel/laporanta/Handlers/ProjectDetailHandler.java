package docotel.laporanta.Handlers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import docotel.laporanta.Models.ClientModel;
import docotel.laporanta.Models.CompanyModel;
import docotel.laporanta.Models.DepartmentModel;
import docotel.laporanta.Models.ProjectModel;
import docotel.laporanta.Listeners.ProjectDetailListener;

/**
 * Created by hanakoizumi on 24/08/16.
 */
public class ProjectDetailHandler {
    private ProjectDetailListener Listener;

    public ProjectDetailHandler(ProjectDetailListener Listener) {
        this.Listener = Listener;
    }

    public void GetProject(Context context, final String token, final int ProjectID) {
        StringRequest ProjectDetailRequest = new StringRequest(Request.Method.GET,
                APIHandler.getProjectsURL() + "/" + String.valueOf(ProjectID),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject JSONProject = new JSONObject(response);
                            ProjectModel Project = new ProjectModel(
                                    JSONProject.getInt("id"),
                                    JSONProject.getString("project_name"),
                                    JSONProject.getDouble("limit"),
                                    Boolean.valueOf(JSONProject.getString("status")),
                                    JSONProject.getInt("year"),
                                    new ClientModel(JSONProject.getJSONObject("client")),
                                    new CompanyModel(JSONProject.getJSONObject("company")),
                                    new DepartmentModel(JSONProject.getJSONObject("department"))
                            );
                            Listener.onSuccess(Project);
                        } catch (JSONException e) {
                            Listener.onFail(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFail(error.getMessage());
                    }
                }) {
            public Map<String,String> getHeaders() {
                Map<String,String> Headers = new HashMap<>();
                Headers.put("Authorization", "Bearer " + token);
                return Headers;
            }
        };
        new RequestHandler().addToRequestQueue(context, ProjectDetailRequest);
    }
}
