package docotel.laporanta.Handlers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import docotel.laporanta.Listeners.EmployeeDetailListener;
import docotel.laporanta.Models.DepartmentModel;
import docotel.laporanta.Models.EmployeeModel;

/**
 * Created by hanakoizumi on 24/08/16.
 */
public class EmployeeDetailHandler {
    private EmployeeDetailListener Listener;

    public EmployeeDetailHandler(EmployeeDetailListener Listener) {
        this.Listener = Listener;
    }

    public void GetEmployee(Context context, final String token, final int EmployeeId) {
        StringRequest GetEmployeeRequest = new StringRequest(Request.Method.GET,
                APIHandler.getEmployeesURL() + "/" + String.valueOf(EmployeeId),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject JSONResponse = new JSONObject(response);
                            JSONObject Department = JSONResponse.getJSONObject("department");
                            SimpleDateFormat DateFormatter = new SimpleDateFormat("yyyy-mm-dd");
                            EmployeeModel Employee = new EmployeeModel(
                                    JSONResponse.getInt("id"),
                                    JSONResponse.getString("name"),
                                    JSONResponse.getString("email"),
                                    DateFormatter.parse(JSONResponse.getString("join_date")),
                                    JSONResponse.getString("position"),
                                    JSONResponse.getString("birth_place"),
                                    DateFormatter.parse(JSONResponse.getString("birth_date")),
                                    new DepartmentModel(Department)
                            );
                            Listener.onSuccess(Employee);
                        } catch (JSONException e) {
                            Listener.onFail(e.getMessage());
                        } catch (ParseException e) {
                            Listener.onFail(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFail(error.getMessage());
                    }
                }) {
            public Map<String,String> getHeaders() {
                Map<String,String> Headers = new HashMap<>();
                Headers.put("Authorization", "Bearer " + token);
                return Headers;
            }
        };
        new RequestHandler().addToRequestQueue(context, GetEmployeeRequest);
    }
}
