package docotel.laporanta.Handlers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.Models.DepartmentModel;

/**
 * Created by VanZa on 8/22/2016.
 */
public class DepartmentHandler extends GetListHandler {
    GetListListener Listener;
    Context context;
    int Method;
    String URL = "";
    Map<String,String> Params = new HashMap<>();
    Map<String,String> Headers = new HashMap<>();

    public DepartmentHandler(@NonNull final GetListListener Listener,
                         @NonNull Context context,
                         @NonNull int Method,
                         @NonNull final String URL,
                         Map<String,String> Params,
                         Map<String,String> Headers) {
        super(Listener);
        this.Listener = Listener;
        this.context = context;
        this.Method = Method;
        this.URL = URL;
        this.Params = Params;
        this.Headers = Headers;
    }

    @Override
    public void GetList(@Nullable Integer Page) {
        if (Headers == null) Headers = new HashMap<>();
        Headers.put("Authorization", "Bearer " + ApplicationHelper.getToken(context));
        String Endpoint = URL;
//        if (Page != null) Endpoint = URL + "?page=" + Page;
//        else Endpoint = URL;
        super.GetList(context, Method, Endpoint, Params, Headers,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray Departments = new JSONArray(response);
                            List<DepartmentModel> DepartmentList = new ArrayList<>();
                            for (int i = 0; i < Departments.length(); i++) {
                                JSONObject Department = Departments.getJSONObject(i);
                                DepartmentList.add(new DepartmentModel(Department));
                            }
                            Listener.onSuccess(DepartmentList, 1);
                        } catch (JSONException e) {
                            Listener.onFail(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFail(error.getLocalizedMessage());
                    }
                }
        );
    }
}
//    private DepartmentListener Listener;
//
//    public DepartmentHandler(DepartmentListener Listener) {
//        this.Listener = Listener;
//    }
//
//    public void GetDepartments(Context context, final String token) {
//        StringRequest GetDepartmentRequest = new StringRequest(Request.Method.GET, APIHandler.getDepartmentsURL(),
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {

//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Listener.onFail(error.getMessage());
//                    }
//                }) {
//            @Override
//            public Map<String,String> getHeaders() {
//                Map<String,String> Headers = new HashMap<>();
//                Headers.put("Authorization", "Bearer " + token);
//                return Headers;
//            }
//        };
//        RequestHandler.getInstance(context).addToRequestQueue(GetDepartmentRequest);
//    }
//}
