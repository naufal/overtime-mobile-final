package docotel.laporanta.Handlers;

/**
 * Created by VanZa on 8/22/2016.
 */
public class APIHandler {
    private static APIHandler ourInstance = new APIHandler();

    public static APIHandler getInstance() {
        return ourInstance;
    }

    private APIHandler() {
    }

    private static String URL = "http://laporanta102.96.lt/api";

    public static String getAuthenticateURL() {
        return URL + "/authenticate";
    }

    public static String getRefreshTokenURL() {
        return getAuthenticateURL() + "/refresh";
    }

    public static String getDepartmentsURL() {
        return URL + "/departments";
    }

    public static String getClientsURL() {
        return URL + "/clients";
    }

    public static String getCompaniesURL() {
        return URL + "/companies";
    }

    public static String getEmployeesURL(){
        return URL + "/employees";
    }

    public static String getProjectsURL(){
        return URL + "/projects";
    }

    public static String getOvertimeURL() {
        return URL + "/overtime";
    }
}
