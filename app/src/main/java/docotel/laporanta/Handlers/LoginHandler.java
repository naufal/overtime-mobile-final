package docotel.laporanta.Handlers;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import docotel.laporanta.Listeners.LoginListener;

/**
 * Created by VanZa on 8/22/2016.
 */
public class LoginHandler {
    LoginListener Listener;

    public LoginHandler(LoginListener Listener) {
        this.Listener = Listener;
    }

    public void Login(Context context, final String username, final String password) {
        StringRequest LoginRequest = new StringRequest(Request.Method.POST, APIHandler.getAuthenticateURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject JSONResponse = new JSONObject(response);
                            String token = JSONResponse.getString("token");
                            Listener.onSuccess(token);
                        } catch (JSONException e) {
                            Listener.onFailure(null, e.getLocalizedMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null) {
                            switch(response.statusCode){
                                case 401:
                                    Listener.onFailure(null, "Authentication Failure");
                                    return;
                            }
                        }
                        Listener.onFailure(null, error.toString());
                    }
                }) {
            @Override
            public Map<String,String> getParams() {
                Map<String,String> Parameters = new HashMap<>();
                Parameters.put("username", username);
                Parameters.put("password", password);
                return Parameters;
            }
        };
        new RequestHandler().addToRequestQueue(context, LoginRequest);
    }
}
