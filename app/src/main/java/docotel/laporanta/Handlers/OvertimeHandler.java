package docotel.laporanta.Handlers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docotel.laporanta.Listeners.OvertimeListener;
import docotel.laporanta.Models.OvertimeModel;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class OvertimeHandler {
    private OvertimeListener Listener;

    public OvertimeHandler(OvertimeListener Listener) {
        this.Listener = Listener;
    }

    public void GetOvertimeList(Context context, final String token, final String OvertimeType) {
        StringRequest GetOvertimeListRequest = new StringRequest(Request.Method.GET,
                APIHandler.getOvertimeURL() + "/" + OvertimeType,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject Response = new JSONObject(response);
                            JSONArray Array = Response.getJSONArray("data");
                            List<OvertimeModel> OvertimeList = new ArrayList<>();
                            for (int i = 0; i < Array.length(); i++) {
                                OvertimeModel Overtime = new OvertimeModel(Array.optJSONObject(i));
                                OvertimeList.add(Overtime);
                            }
                            Listener.onSuccess(OvertimeList);
                        } catch (JSONException e) {
                            Listener.onFail(e.getMessage());
                        } catch (ParseException e) {
                            Listener.onFail(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFail(error.getMessage());
                    }
                }) {
            @Override
            public Map<String,String> getHeaders() {
                Map<String,String> Headers = new HashMap<>();
                Headers.put("Authorization", "Bearer " + token);
                return Headers;
            }
        };
        new RequestHandler().addToRequestQueue(context, GetOvertimeListRequest);
    }
}
