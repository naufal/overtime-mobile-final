package docotel.laporanta.Handlers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import docotel.laporanta.Listeners.CreateOvertimeListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docotel.laporanta.Models.EmployeeModel;
import docotel.laporanta.Models.ProjectModel;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class CreateOvertimeHandler {
    private CreateOvertimeListener Listener;

    public CreateOvertimeHandler(CreateOvertimeListener Listener){
        this.Listener = Listener;
    }

    public void GetEmployees (Context context, final String token){
        StringRequest GetEmployeesRequest = new StringRequest(Request.Method.GET,
                APIHandler.getEmployeesURL() + "/all",
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){
                        try {
                            JSONArray Result = new JSONArray(response);
                            List<EmployeeModel> EmployeeList = new ArrayList<>();
                            for (int i = 0; i < Result.length(); i++) {
                                JSONObject Employee = Result.getJSONObject(i);
                                EmployeeList.add(new EmployeeModel(
                                        Employee.getInt("id"),
                                        Employee.getString("position"),
                                        Employee.getString("name")
                                ));
                            }
                            Listener.onSuccessFetchEmployeeList(EmployeeList);
                        } catch (JSONException e) {
                            Listener.onFailFetchEmployeeList(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFailFetchEmployeeList(error.getMessage());
                    }
                }) {
            public Map<String,String> getHeaders() {
                Map<String,String> Headers = new HashMap<>();
                Headers.put("Authorization", "Bearer " + token);
                return Headers;
            }
        };
        new RequestHandler().addToRequestQueue(context, GetEmployeesRequest);
    }

    public void GetProjects (Context context, final String token){
        StringRequest GetProjectsRequest = new StringRequest(Request.Method.GET,
                APIHandler.getProjectsURL(),
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){
                        try {
                            JSONObject Result = new JSONObject(response);
                            JSONArray data = Result.getJSONArray("data");
                            List<ProjectModel> ProjectList = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject Project = data.getJSONObject(i);
                                ProjectList.add(new ProjectModel(
                                        Project.getInt("id"),
                                        Project.getString("project_name")
                                ));
                            }
                            Listener.onSuccessFetchProjectList(ProjectList);
                        } catch (JSONException e) {
                            Listener.onFailFetchEmployeeList(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFailFetchEmployeeList(error.getMessage());
                    }
                }) {
            public Map<String,String> getHeaders() {
                Map<String,String> Headers = new HashMap<>();
                Headers.put("Authorization", "Bearer " + token);
                return Headers;
            }
        };
        new RequestHandler().addToRequestQueue(context, GetProjectsRequest);
    }
}
