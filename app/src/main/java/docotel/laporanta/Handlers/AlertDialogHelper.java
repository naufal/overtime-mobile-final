package docotel.laporanta.Handlers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class AlertDialogHelper {
    private static AlertDialogHelper ourInstance = new AlertDialogHelper();

    public static AlertDialogHelper getInstance() {
        return ourInstance;
    }

    private AlertDialogHelper() {
    }

    private static ProgressDialog progressDialog;

    public static void showAlertDialog(Context context,
                                       String title,
                                       String message,
                                       String buttonText,
                                       DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(buttonText, onClickListener)
                .show();
    }

    public static void showAskDialog(Context context,
                                     String title,
                                     String message,
                                     String PositiveButtonText,
                                     DialogInterface.OnClickListener onPositiveButtonClickListener,
                                     String NegativeButtonText,
                                     DialogInterface.OnClickListener onNegativeButtonClickListener,
                                     boolean isCancelable) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(PositiveButtonText, onPositiveButtonClickListener)
                .setNegativeButton(NegativeButtonText, onNegativeButtonClickListener)
                .setCancelable(isCancelable)
                .show();
    }

    public static void showProgressDialog(Context context,
                                          String title,
                                          String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
        }
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public static void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
