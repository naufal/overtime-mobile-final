package docotel.laporanta.Handlers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import docotel.laporanta.Listeners.RefreshTokenListener;

/**
 * Created by VanZa on 8/22/2016.
 */
public class RefreshTokenHandler {
    RefreshTokenListener Listener;

    public RefreshTokenHandler(RefreshTokenListener Listener) {
        this.Listener = Listener;
    }

    public void Refresh(Context context, final String token) {
        StringRequest RefreshTokenRequest = new StringRequest(Request.Method.GET, APIHandler.getRefreshTokenURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject JSONResponse = new JSONObject(response);
                            String token = JSONResponse.getString("token");
                            Listener.onSuccessRefreshToken(token);
                        } catch (JSONException e) {
                            Listener.onFailRefreshToken(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Listener.onFailRefreshToken(error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> Headers = new HashMap<>();
                Headers.put("Authorization", "Bearer " + token);
                return Headers;
            }
        };
        new RequestHandler().addToRequestQueue(context, RefreshTokenRequest);
    }
}
