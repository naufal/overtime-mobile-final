package docotel.laporanta.Listeners;

import java.util.List;

import docotel.laporanta.Models.DepartmentModel;

/**
 * Created by VanZa on 8/22/2016.
 */
public interface DepartmentListener {
    void onSuccess(List<DepartmentModel> Departments);
    void onFail(String message);
}
