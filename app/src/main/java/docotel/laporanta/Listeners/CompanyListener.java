package docotel.laporanta.Listeners;

import java.util.List;

import docotel.laporanta.Models.CompanyModel;

/**
 * Created by VanZa on 8/22/2016.
 */
public interface CompanyListener {
    void onSuccess(List<CompanyModel> Companies, int TotalPage);
    void onFail(String message);
}
