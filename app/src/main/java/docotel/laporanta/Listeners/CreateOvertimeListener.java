package docotel.laporanta.Listeners;

import java.util.List;

import docotel.laporanta.Models.EmployeeModel;
import docotel.laporanta.Models.ProjectModel;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public interface CreateOvertimeListener {
    void onSuccessFetchEmployeeList(List<EmployeeModel> EmployeeList);
    void onSuccessFetchProjectList(List<ProjectModel> ProjectList);
    void onFailFetchEmployeeList(String message);
}
