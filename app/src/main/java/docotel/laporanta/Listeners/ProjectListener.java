package docotel.laporanta.Listeners;

import java.util.List;

import docotel.laporanta.Models.ProjectModel;

/**
 * Created by VanZa on 8/23/2016.
 */
public interface ProjectListener {
    void onSuccess(List<ProjectModel> Projects);
    void onFail(String message);
}
