package docotel.laporanta.Listeners;

import docotel.laporanta.Models.ProjectModel;

/**
 * Created by hanakoizumi on 24/08/16.
 */
public interface ProjectDetailListener {
    void onSuccess(ProjectModel project);
    void onFail(String message);
}
