package docotel.laporanta.Listeners;

import java.util.List;

import docotel.laporanta.Models.ClientModel;

/**
 * Created by VanZa on 8/22/2016.
 */
public interface ClientListener {
    void onSuccess(List<ClientModel> Clients, int TotalPage);
    void onFail(String message);
}
