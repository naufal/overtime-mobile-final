package docotel.laporanta.Listeners;

import java.util.List;

import docotel.laporanta.Models.EmployeeModel;

/**
 * Created by VanZa on 8/23/2016.
 */
public interface EmployeeListener {
    void onSuccess (List<EmployeeModel> Employees, int TotalPage);
    void onFail(String message);
}
