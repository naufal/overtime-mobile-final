package docotel.laporanta.Listeners;

/**
 * Created by VanZa on 8/22/2016.
 */
public interface RefreshTokenListener {
    void onSuccessRefreshToken(String token);
    void onFailRefreshToken(String message);
}
