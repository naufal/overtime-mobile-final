package docotel.laporanta.Listeners;

import android.widget.EditText;

/**
 * Created by VanZa on 8/22/2016.
 */
public interface LoginListener {
    void onSuccess(String token);
    void onFailure(EditText editText, String errorMessage);
}
