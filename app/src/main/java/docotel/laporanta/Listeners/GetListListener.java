package docotel.laporanta.Listeners;

import java.util.List;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public interface GetListListener {
    void onSuccess(List<?> List, int TotalPage);
    void onFail(String message);
}
