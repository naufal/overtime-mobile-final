package docotel.laporanta.Listeners;

import java.util.List;

import docotel.laporanta.Models.OvertimeModel;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public interface OvertimeListener {
    void onSuccess(List<OvertimeModel> OvertimeList);
    void onFail(String message);
}
