package docotel.laporanta.Listeners;

import docotel.laporanta.Models.EmployeeModel;

/**
 * Created by hanakoizumi on 24/08/16.
 */
public interface EmployeeDetailListener {
    void onSuccess(EmployeeModel Employee);
    void onFail(String message);
}
