package docotel.laporanta.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by VanZa on 8/22/2016.
 */
public class CompanyModel {
    private int id;
    private String prefix;
    private String company_name;

    public CompanyModel(JSONObject object) throws JSONException {
        id = object.getInt("id");
        prefix = object.getString("prefix");
        company_name = object.getString("company_name");
    }

    public int getId() {
        return id;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getCompanyName() {
        return company_name;
    }

    @Override
    public String toString() {
        return company_name;
    }
}
