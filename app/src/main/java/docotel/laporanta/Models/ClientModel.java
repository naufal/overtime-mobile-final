package docotel.laporanta.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by VanZa on 8/22/2016.
 */
public class ClientModel {
    private int id;
    private String prefix;
    private String client_name;

    public ClientModel(JSONObject object) throws JSONException {
        id = object.getInt("id");
        prefix = object.getString("prefix");
        client_name = object.getString("client_name");
    }

    public int getId() {
        return id;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getClientName() {
        return client_name;
    }

    @Override
    public String toString() {
        return client_name;
    }
}
