package docotel.laporanta.Models;

import java.util.Date;

/**
 * Created by VanZa on 8/23/2016.
 */
public class EmployeeModel {
    private int id;
    private String position;
    private String employee_name;
    private String email;
    private Date join_date;
    private String birth_place;
    private Date birth_date;
    private DepartmentModel department;

    public EmployeeModel(int id, String position, String employee_name){
        this.id = id;
        this.position = position;
        this.employee_name = employee_name;
    }

    public EmployeeModel(int id, String employee_name, String email, Date join_date, String position, String birth_place, Date birth_date, DepartmentModel department) {
        this.id = id;
        this.position = position;
        this.employee_name = employee_name;
        this.email = email;
        this.join_date = join_date;
        this.birth_place = birth_place;
        this.birth_date = birth_date;
        this.department = department;
    }

//    public EmployeeModel(JSONObject object) throws JSONException, ParseException {
//        String s_joindate, s_birthdate;
//        SimpleDateFormat DateFormatter = new SimpleDateFormat("yyyy-MM-dd");
//        this.id = object.getInt("id");
//        this.employee_name = object.getString("name");
//        this.email = object.getString("email");
//        s_joindate = object.getString("join_date");
//        s_birthdate = object.getString("birth_date");
//        this.join_date = DateFormatter.parse(s_joindate);
//        this.position = object.getString("position");
//        this.birth_place = object.getString("birth_place");
//        this.birth_date = DateFormatter.parse(s_birthdate);
//    }

    public int getId() {
        return id;
    }

    public String getPosition() {
        return position;
    }

    public String getEmployeeName() {
        return employee_name;
    }

    public Date getJoinDate() {
        return join_date;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthPlace() {
        return birth_place;
    }

    public Date getBirthDate() {
        return birth_date;
    }

    public DepartmentModel getDepartment() {
        return department;
    }

    @Override
    public String toString() {
        return employee_name;
    }
}
