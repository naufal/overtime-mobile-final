package docotel.laporanta.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class OvertimeModel {
    public enum OvertimeStatus {
        WAITING,
        APPROVED,
        REJECTED
    }

    private Integer id;
    private Date date;
    private Date clock_in;
    private Date clock_out;
    private OvertimeStatus approved;
    private String type;
    private EmployeeModel employee;
    private ProjectModel project;
    private EmployeeModel created_by;
    private Date created_at;
    private Date updated_at;

    public OvertimeModel(JSONObject json) throws JSONException, ParseException {
        String s_date, s_clockin, s_clockout, s_createdat, s_updateat, s_approved;
        SimpleDateFormat DateFormatter = new SimpleDateFormat("yyyy-M-dd");
        SimpleDateFormat TimeFormatter = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat DateTimeFormatter = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
        id = json.getInt("id");
        s_date = json.getString("date");
        s_clockin = json.getString("clock_in");
        s_clockout = json.getString("clock_out");
        s_createdat = json.getString("created_at");
        s_updateat = json.getString("updated_at");
        s_approved = json.getString("approved");
        date = DateFormatter.parse(s_date);
        clock_in = TimeFormatter.parse(s_clockin);
        clock_out = TimeFormatter.parse(s_clockout);
        created_at = DateTimeFormatter.parse(s_createdat);
        updated_at = DateTimeFormatter.parse(s_updateat);
        if (s_approved.equals("null")) {
            approved = OvertimeStatus.WAITING;
        } else if (s_approved.equals("0")) {
            approved = OvertimeStatus.REJECTED;
        } else if (s_approved.equals("1")){
            approved = OvertimeStatus.APPROVED;
        } else {
            approved = OvertimeStatus.REJECTED;
        }
        type = json.getString("overtime_type_id");
        JSONObject Employee = json.getJSONObject("employee");
        employee = new EmployeeModel(
                Employee.getInt("id"),
                Employee.getString("position"),
                Employee.getString("name")
        );
        JSONObject Project = json.getJSONObject("project");
        project = new ProjectModel(
                Project.getInt("id"),
                Project.getString("project_name")
        );
        JSONObject CreatedBy = json.getJSONObject("created_by");
        created_by = new EmployeeModel(
                CreatedBy.getInt("id"),
                CreatedBy.getString("position"),
                CreatedBy.getString("name")
        );
    }

    public Integer getID() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public Date getClockIn() {
        return clock_in;
    }

    public Date getClockOut() {
        return clock_out;
    }

    public OvertimeStatus getOvertimeStatus() {
        return approved;
    }

    public String getOvertimeType() {
        return type;
    }

    public EmployeeModel getEmployee() {
        return employee;
    }

    public ProjectModel getProject() {
        return project;
    }

    public EmployeeModel getCreatedBy() {
        return created_by;
    }

    public Date getCreatedAt() {
        return created_at;
    }

    public Date getUpdatedAt() {
        return updated_at;
    }
}
