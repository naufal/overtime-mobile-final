package docotel.laporanta.Models;

/**
 * Created by VanZa on 8/23/2016.
 */
public class ProjectModel {
    private Integer id;
    private String project_name;
    private Double limit;
    private Boolean status;
    private Integer year;
    private ClientModel client;
    private CompanyModel company;
    private DepartmentModel department;

    public ProjectModel(Integer id, String project_name) {
        this.id = id;
        this.project_name = project_name;
    }

    public ProjectModel(Integer id,
                        String project_name,
                        Double limit,
                        Boolean status,
                        Integer year,
                        ClientModel client,
                        CompanyModel company,
                        DepartmentModel department) {
        this.id = id;
        this.project_name = project_name;
        this.limit = limit;
        this.status = status;
        this.year = year;
        this.client = client;
        this.company = company;
        this.department = department;
    }

    public Integer getId() {
        return id;
    }

    public String getProjectName() {
        return project_name;
    }

    public Double getLimit() {
        return limit;
    }

    public Boolean getStatus() {
        return status;
    }

    public Integer getYear() {
        return year;
    }

    public ClientModel getClient() {
        return client;
    }

    public CompanyModel getCompany() {
        return company;
    }

    public DepartmentModel getDepartment() {
        return department;
    }

    @Override
    public String toString() {
        return project_name;
    }
}
