package docotel.laporanta.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by VanZa on 8/22/2016.
 */
public class DepartmentModel {
    private int id;
    private String prefix;
    private String department_name;

    public DepartmentModel(JSONObject object) throws JSONException {
        id = object.getInt("id");
        prefix = object.getString("prefix");
        department_name = object.getString("department_name");
    }

    public int getId(){
        return id;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getDepartmentName() {
        return department_name;
    }

    @Override
    public String toString() {
        return department_name;
    }
}
