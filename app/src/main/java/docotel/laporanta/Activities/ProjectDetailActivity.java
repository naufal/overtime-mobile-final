package docotel.laporanta.Activities;

import android.app.ProgressDialog;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Locale;

import docotel.laporanta.Handlers.ApplicationHelper;
import docotel.laporanta.Models.ProjectModel;
import docotel.laporanta.Handlers.ProjectDetailHandler;
import docotel.laporanta.Listeners.ProjectDetailListener;
import docotel.laporanta.R;

public class ProjectDetailActivity extends ProjectBaseActivity implements ProjectDetailListener {
    private int ProjectID;
    private ProjectDetailHandler ProjectDetailHelper = new ProjectDetailHandler(this);
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
        setActivityToolbar((Toolbar) findViewById(R.id.ProjectDetailToolbar), "Project Detail");

        Bundle Extras = getIntent().getExtras();
        ProjectID = Extras.getInt("ProjectID");

        ProjectDetailHelper.GetProject(this, ApplicationHelper.getToken(this), ProjectID);
        showProgressDialog("Loading...");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onSuccess(ProjectModel project) {
        TextView prod_ProjectName = (TextView) findViewById(R.id.prod_ProjectName);
        TextView prod_Limit = (TextView) findViewById(R.id.prod_Limit);
        TextView prod_Status = (TextView) findViewById(R.id.prod_Status);
        TextView prod_Year = (TextView) findViewById(R.id.prod_Year);
        TextView prod_Client = (TextView) findViewById(R.id.prod_ClientName);
        TextView prod_Company = (TextView) findViewById(R.id.prod_CompanyName);
        TextView prod_Department = (TextView) findViewById(R.id.prod_DepartmentName);

        prod_ProjectName.setText(project.getProjectName());
        prod_Limit.setText(formatDecimal(project.getLimit()));
        if (project.getStatus()) {
            prod_Status.setText("Finished");
        } else {
            prod_Status.setText("Unfinished");
        }
        prod_Year.setText(String.valueOf(project.getYear()));
        prod_Client.setText(project.getClient().getClientName());
        prod_Company.setText(project.getCompany().getCompanyName());
        prod_Department.setText(project.getDepartment().getDepartmentName());
        hideProgressDialog();
    }

    @Override
    public void onFail(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(message)
                .show();
        hideProgressDialog();
    }

    private String formatDecimal(Double number) {
        return "IDR " + String.format(Locale.GERMAN, "%,.2f", number);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
        }
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
