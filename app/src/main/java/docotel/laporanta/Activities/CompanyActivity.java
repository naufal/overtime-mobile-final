package docotel.laporanta.Activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.android.volley.Request;

import docotel.laporanta.Adapters.CompanyAdapter;
import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.Handlers.APIHandler;
import docotel.laporanta.Handlers.CompanyHandler;
import docotel.laporanta.R;

public class CompanyActivity extends ListActivity implements GetListListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        setActivityToolbar((Toolbar) findViewById(R.id.CompanyToolbar), "Company List");
        setListHandler(new CompanyHandler(this, this, Request.Method.GET,
                APIHandler.getCompaniesURL(), null, null));
        setListView((ListView) findViewById(R.id.CompanyListView), new CompanyAdapter(this, list), null, null);
    }
}