package docotel.laporanta.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ProgressBar;
import docotel.laporanta.Handlers.ApplicationHelper;
import docotel.laporanta.Handlers.LoginHandler;
import docotel.laporanta.Listeners.LoginListener;
import docotel.laporanta.R;

public class LoginActivity extends AppCompatActivity implements LoginListener {
    private ProgressBar progressBar;
    private EditText inputUsername;
    private EditText inputPassword;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        final LoginHandler LoginHelper = new LoginHandler(this);
        inputUsername = (EditText) findViewById(R.id.inputUsername);
        inputPassword = (EditText) findViewById(R.id.inputPassword);

        loginButton = (Button) findViewById(R.id.buttonLogin);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (formValid()) {
                    lockForm(true);
                    LoginHelper.Login(view.getContext(), inputUsername.getText().toString(), inputPassword.getText().toString());
                }
            }
        });
    }

    @Override
    public void onSuccess(String token) {
        ApplicationHelper.setToken(this, token);
//        startActivity(new Intent(this, DashboardActivity.class));
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onFailure(@Nullable EditText editText, @NonNull String errorMessage) {
        if (editText == null) {
            onFailure(inputUsername, errorMessage);
            return;
        }
        lockForm(false);
        editText.setError(errorMessage);
        editText.requestFocus();
        progressBar.setVisibility(View.GONE);
    }

    private void lockForm(boolean lock) {
        inputUsername.setEnabled(!lock);
        inputPassword.setEnabled(!lock);
        if (lock) {
            progressBar.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            loginButton.setVisibility(View.VISIBLE);
        }
    }

    private boolean formValid() {
        if (inputPassword.getText().toString().equals("")
                || inputUsername.getText().toString().equals("")) {
            if (inputPassword.getText().toString().equals("")) {
                onFailure(inputPassword, "Password harus diisi.");
            }
            if (inputUsername.getText().toString().equals("")) {
                onFailure(inputUsername, "Username harus diisi.");
            }
            return false;
        }
        else return true;
    }
}
