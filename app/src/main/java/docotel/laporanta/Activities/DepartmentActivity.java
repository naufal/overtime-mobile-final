package docotel.laporanta.Activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.android.volley.Request;

import docotel.laporanta.Adapters.DepartmentAdapter;
import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.Handlers.APIHandler;
import docotel.laporanta.Handlers.DepartmentHandler;
import docotel.laporanta.R;

public class DepartmentActivity extends ListActivity implements GetListListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department);
        setActivityToolbar((Toolbar) findViewById(R.id.DepartmentToolbar), "Department List");
        setListHandler(new DepartmentHandler(this, this, Request.Method.GET,
                APIHandler.getDepartmentsURL(), null, null));
        setListView((ListView) findViewById(R.id.DepartmentListView), new DepartmentAdapter(this, list), null, null);
    }
}
