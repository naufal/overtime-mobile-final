package docotel.laporanta.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class ProjectBaseActivity extends AppCompatActivity {
    private Toolbar ActivityToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setActivityToolbar(Toolbar toolbar, String title) {
        ActivityToolbar = toolbar;
        setSupportActionBar(ActivityToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
        }
        return true;
    }
}
