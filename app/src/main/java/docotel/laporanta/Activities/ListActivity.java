package docotel.laporanta.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import docotel.laporanta.Handlers.AlertDialogHelper;
import docotel.laporanta.Adapters.ProjectBaseAdapter;
import docotel.laporanta.Handlers.GetListHandler;
import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.R;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public abstract class ListActivity extends ProjectBaseActivity implements GetListListener {
    protected GetListHandler getListHandler;
    protected ProjectBaseAdapter Adapter;
    protected List list = new ArrayList<>();
    protected ListView listView;
    protected ProgressBar ProgressBarFooter;
    protected int CurrentPage = 1;
    protected int TotalPage = -1;
    protected boolean isCurrentlyLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View Footer = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.progressbar, null, false);
        ProgressBarFooter = (ProgressBar) Footer.findViewById(R.id.ProgressBar);
        ProgressBarFooter.setVisibility(View.GONE);
    }

    protected void setListView(@NonNull ListView listView,
                               @NonNull ProjectBaseAdapter Adapter,
                               AbsListView.OnScrollListener onScrollListener,
                               AbsListView.OnItemClickListener onItemClickListener) {
        this.listView = listView;
        this.listView.addFooterView(ProgressBarFooter);
        this.Adapter = Adapter;
        listView.setAdapter(this.Adapter);
        if (onScrollListener != null) {
            listView.setOnScrollListener(onScrollListener);
        } else {
            listView.setOnScrollListener(
                    new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView absListView, int i) {
                        }

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if ((firstVisibleItem + visibleItemCount == totalItemCount)
                                    && !isCurrentlyLoading
                                    && (CurrentPage == 1 || CurrentPage <= TotalPage)) {
                                isCurrentlyLoading = true;
                                ProgressBarFooter.setVisibility(View.VISIBLE);
                                getListHandler.GetList(CurrentPage);
                                CurrentPage = CurrentPage + 1;
                            }
                        }
                    }
            );
        }
        listView.setOnItemClickListener(onItemClickListener);
    }

    protected void setListHandler(GetListHandler getListListener) {
        this.getListHandler = getListListener;
    }

    @Override
    public void onSuccess(List<?> list, int TotalPage) {
        this.list.addAll(list);
        this.TotalPage = TotalPage;
        isCurrentlyLoading = false;
        ProgressBarFooter.setVisibility(View.GONE);
        if (CurrentPage > TotalPage) listView.removeFooterView(ProgressBarFooter);
        Adapter.notifyDataSetChanged();
    }

    @Override
    public void onFail(String message) {
        if (message == null) message = "Connection problem, try again later.";
        AlertDialogHelper.showAlertDialog(this, "Error", message, "Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
    }
}
