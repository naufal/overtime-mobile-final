package docotel.laporanta.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docotel.laporanta.Handlers.AlertDialogHelper;
import docotel.laporanta.Adapters.OvertimeAdapter;
import docotel.laporanta.Handlers.ApplicationHelper;
import docotel.laporanta.Handlers.APIHandler;
import docotel.laporanta.Handlers.OvertimeHandler;
import docotel.laporanta.Handlers.RequestHandler;
import docotel.laporanta.Listeners.OvertimeListener;
import docotel.laporanta.Models.OvertimeModel;
import docotel.laporanta.R;

public class OvertimeActivity extends ProjectBaseActivity {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overtime);
        setActivityToolbar((Toolbar) findViewById(R.id.toolbar), "Overtime List");
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toCreateOvertimeActivity();
            }
        });
    }

    @Override
    public void onRestart() {
        super.onRestart();
        startActivity(new Intent(this, OvertimeActivity.class));
        finish();
    }

    private void toCreateOvertimeActivity() {
        startActivity(new Intent(this, CreateOvertimeActivity.class));
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements OvertimeListener {
        private OvertimeHandler OvertimeHelper = new OvertimeHandler(this);
        private SharedPreferences Preferences;
        private ListView OvertimeListView;
        private OvertimeAdapter Adapter;
        private List<OvertimeModel> OvertimeListWeekend = new ArrayList<>();
        private List<OvertimeModel> OvertimeListWeekday = new ArrayList<>();
        private List<OvertimeModel> OvertimeListHoliday = new ArrayList<>();
        private View FooterListView;
        private ProgressBar progressBar;
        private int CurrentPageWeekday = 1;
        private int TotalPageWeekday = -1;
        private int CurrentPageWeekend = 1;
        private int TotalPageWeekend = -1;
        private int CurrentPageHoliday = 1;
        private int TotalPageHoliday = -1;
        private boolean isCurrentlyLoadingWeekendData = false;
        private boolean isCurrentlyLoadingWeekdayData = false;
        private boolean isCurrentlyLoadingHolidayData = false;
        private String URL;
        private ProgressDialog progressDialog;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_overtime, container, false);
            OvertimeListView = (ListView) rootView.findViewById(R.id.OvertimeList);

            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1: //weekday
                    Adapter = new OvertimeAdapter(this.getActivity(), OvertimeListWeekday);
                    break;
                case 2: //weekend
                    Adapter = new OvertimeAdapter(this.getActivity(), OvertimeListWeekend);
                    break;
                case 3: //holiday
                    Adapter = new OvertimeAdapter(this.getActivity(), OvertimeListHoliday);
                    break;
            }
            OvertimeListView.setAdapter(Adapter);
            FooterListView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.progressbar, null, false);
            progressBar = (ProgressBar) FooterListView.findViewById(R.id.ProgressBar);
            progressBar.setVisibility(View.GONE);
            OvertimeListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView absListView, int i) {}
                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                        case 1:
                            if ((firstVisibleItem + visibleItemCount == totalItemCount)
                                    && !isCurrentlyLoadingWeekdayData
                                    && (CurrentPageWeekday == 1 || CurrentPageWeekday <= TotalPageWeekday)) {
                                LoadDataWeekday();
                                CurrentPageWeekday = CurrentPageWeekday + 1;
                            }
                            break;
                        case 2:
                            if ((firstVisibleItem + visibleItemCount == totalItemCount)
                                    && !isCurrentlyLoadingWeekendData
                                    && (CurrentPageWeekend == 1 || CurrentPageWeekend <= TotalPageWeekend)) {
                                LoadDataWeekend();
                                CurrentPageWeekend = CurrentPageWeekend + 1;
                            }
                            break;
                        case 3:
                            if ((firstVisibleItem + visibleItemCount == totalItemCount)
                                    && !isCurrentlyLoadingHolidayData
                                    && (CurrentPageHoliday == 1 || CurrentPageHoliday <= TotalPageHoliday)) {
                                LoadDataHoliday();
                                CurrentPageHoliday = CurrentPageHoliday + 1;
                            }
                            break;
                    }
                }
            });
            OvertimeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    OvertimeModel Overtime = (OvertimeModel) OvertimeListView.getItemAtPosition(i);
                    final int id = Overtime.getID();
                    AlertDialogHelper.showAskDialog(PlaceholderFragment.this.getActivity(), null, "Setujui atau tidak pengajuan Overtime?", "Approve",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    URL = APIHandler.getOvertimeURL() + "/" + String.valueOf(id) + "/approve";
                                    ApprovalRequest();
                                    showProgressDialog("Loading...");
                                }
                            }, "Reject",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    URL = APIHandler.getOvertimeURL() + "/" + String.valueOf(id) + "/reject";
                                    ApprovalRequest();
                                    showProgressDialog("Loading...");
                                }
                            }, true
                    );
                }
            });
            return rootView;
        }

        DialogInterface.OnClickListener DismissAlert() {
            return new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = PlaceholderFragment.this.getActivity().getIntent();
                    PlaceholderFragment.this.getActivity().finish();
                    startActivity(intent);
                }
            };
        }

        private void ApprovalRequest() {
            StringRequest ApprovalRequest = new StringRequest(Request.Method.PUT, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject Response = new JSONObject(response);
                                boolean success = Response.getBoolean("success");
                                if (success) {
                                    AlertDialogHelper.showAlertDialog(PlaceholderFragment.this.getActivity(),
                                            null, "Success", "Close", DismissAlert());
                                } else {
                                    String message = Response.getString("message");
                                    AlertDialogHelper.showAlertDialog(PlaceholderFragment.this.getActivity(),
                                            null, message, "Close", null);
                                }
                            } catch (JSONException e) {
                                AlertDialogHelper.showAlertDialog(PlaceholderFragment.this.getActivity(),
                                        null, e.getMessage(), "Close", null);
                            }
                            hideProgressDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            AlertDialogHelper.showAlertDialog(PlaceholderFragment.this.getActivity(),
                                    "Error",
                                    error.getMessage(),
                                    "OK", null);
                        }
                    }
            ) {
                @Override
                public Map<String,String> getHeaders() {
                    Map<String,String> Headers = new HashMap<>();
                    Headers.put("Authorization", "Bearer " + ApplicationHelper.getToken(PlaceholderFragment.this.getContext()));
                    return Headers;
                }
            };
            new RequestHandler().addToRequestQueue(PlaceholderFragment.this.getContext(), ApprovalRequest);
            hideProgressDialog();
        }

        private void LoadDataWeekday() {
            OvertimeListView.addFooterView(progressBar);
            isCurrentlyLoadingWeekdayData = true;
            progressBar.setVisibility(View.VISIBLE);
            OvertimeHelper.GetOvertimeList(getContext(), ApplicationHelper.getToken(getContext()), "weekday");
        }

        private void LoadDataWeekend() {
            OvertimeListView.addFooterView(progressBar);
            isCurrentlyLoadingWeekdayData = true;
            progressBar.setVisibility(View.VISIBLE);
            OvertimeHelper.GetOvertimeList(getContext(), ApplicationHelper.getToken(getContext()), "weekend");
        }

        private void LoadDataHoliday() {
            OvertimeListView.addFooterView(progressBar);
            isCurrentlyLoadingWeekdayData = true;
            progressBar.setVisibility(View.VISIBLE);
            OvertimeHelper.GetOvertimeList(getContext(), ApplicationHelper.getToken(getContext()), "holiday");
        }

        @Override
        public void onSuccess(List<OvertimeModel> OvertimeList) {
            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    OvertimeListWeekday.addAll(OvertimeList);
                    isCurrentlyLoadingWeekdayData = false;
                    break;
                case 2:
                    OvertimeListWeekend.addAll(OvertimeList);
                    isCurrentlyLoadingWeekendData = false;
                    break;
                case 3:
                    OvertimeListHoliday.addAll(OvertimeList);
                    isCurrentlyLoadingHolidayData = false;
                    break;
            }
            Adapter.notifyDataSetChanged();
            OvertimeListView.removeFooterView(progressBar);
        }

        @Override
        public void onFail(String message) {
            hideProgressDialog();
            AlertDialogHelper.showAlertDialog(getActivity(), "Error", message, "Close", null);
        }

        private void showProgressDialog(String message) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(PlaceholderFragment.this.getActivity());
                progressDialog.setMessage(message);
            }
            progressDialog.show();
        }

        private void hideProgressDialog() {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Weekday";
                case 1:
                    return "Weekend";
                case 2:
                    return "Holiday";
            }
            return null;
        }
    }
}
