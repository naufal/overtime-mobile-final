package docotel.laporanta.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;

import docotel.laporanta.Adapters.ProjectAdapter;
import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.Handlers.APIHandler;
import docotel.laporanta.Handlers.ProjectHandler;
import docotel.laporanta.Models.ProjectModel;
import docotel.laporanta.R;

public class ProjectActivity extends ListActivity implements GetListListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        setActivityToolbar((Toolbar) findViewById(R.id.ProjectToolbar), "Project List");
        setListHandler(new ProjectHandler(this, this, Request.Method.GET,
                APIHandler.getProjectsURL(), null, null));
        setListView((ListView) findViewById(R.id.ProjectListView), new ProjectAdapter(this, list), null,
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        ProjectModel Project = (ProjectModel) listView.getItemAtPosition(i);
                        Intent intent = new Intent(ProjectActivity.this, ProjectDetailActivity.class);
                        intent.putExtra("ProjectID", Project.getId());
                        startActivity(intent);
                    }
                });
    }
}