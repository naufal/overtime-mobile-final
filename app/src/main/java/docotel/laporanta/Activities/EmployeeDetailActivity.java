package docotel.laporanta.Activities;

import android.app.ProgressDialog;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import docotel.laporanta.Handlers.ApplicationHelper;
import docotel.laporanta.Handlers.EmployeeDetailHandler;
import docotel.laporanta.Listeners.EmployeeDetailListener;
import docotel.laporanta.Models.EmployeeModel;
import docotel.laporanta.R;

public class EmployeeDetailActivity extends ProjectBaseActivity implements EmployeeDetailListener {
    private TextView empd_EmployeeName;
    private TextView empd_BirthPlace;
    private TextView empd_BirthDate;
    private TextView empd_Position;
    private TextView empd_Department;
    private TextView empd_JoinDate;
    private int EmployeeID;
    private EmployeeDetailHandler EmployeeDetailHelper = new EmployeeDetailHandler(this);
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_detail);
        setActivityToolbar((Toolbar) findViewById(R.id.EmployeeDetailToolbar), "Employee Detail");

        Bundle Extras = getIntent().getExtras();
        EmployeeID = Extras.getInt("EmployeeID");

        empd_EmployeeName = (TextView) findViewById(R.id.empd_EmployeeName);
        empd_BirthPlace = (TextView) findViewById(R.id.empd_BirthPlace);
        empd_BirthDate = (TextView) findViewById(R.id.empd_BirthDate);
        empd_Position = (TextView) findViewById(R.id.empd_Position);
        empd_Department = (TextView) findViewById(R.id.empd_Department);
        empd_JoinDate = (TextView) findViewById(R.id.empd_JoinDate);

        EmployeeDetailHelper.GetEmployee(this, ApplicationHelper.getToken(this), EmployeeID);
        showProgressDialog("Loading...");
    }

    @Override
    public void onSuccess(EmployeeModel Employee) {
        SimpleDateFormat DateFormatter = new SimpleDateFormat("dd MMMM yyyy");
        empd_EmployeeName.setText(Employee.getEmployeeName());
        empd_BirthPlace.setText(Employee.getBirthPlace());
        empd_BirthDate.setText(DateFormatter.format(Employee.getBirthDate()));
        empd_Position.setText(Employee.getPosition());
        empd_Department.setText(Employee.getDepartment().getDepartmentName());
        empd_JoinDate.setText(DateFormatter.format(Employee.getJoinDate()));
        hideProgressDialog();
    }

    @Override
    public void onFail(String message) {
        hideProgressDialog();
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(message)
                .show();
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
        }
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
