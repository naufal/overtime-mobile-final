package docotel.laporanta.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import docotel.laporanta.Handlers.ApplicationHelper;
import docotel.laporanta.R;
import docotel.laporanta.Handlers.RefreshTokenHandler;
import docotel.laporanta.Listeners.RefreshTokenListener;

public class SplashScreen extends AppCompatActivity implements RefreshTokenListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        String token = ApplicationHelper.getToken(this);
        if (token != "") {
            new RefreshTokenHandler(this).Refresh(this, token);
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    @Override
    public void onSuccessRefreshToken(String token) {
        ApplicationHelper.setToken(this, token);
        toMainActivity();
    }

    @Override
    public void onFailRefreshToken(String message) {
        toLoginActivity();
    }

    private void toLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void toMainActivity() {
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }
}
