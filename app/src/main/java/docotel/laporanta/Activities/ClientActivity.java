package docotel.laporanta.Activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.android.volley.Request;

import docotel.laporanta.Adapters.ClientAdapter;
import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.Handlers.APIHandler;
import docotel.laporanta.Handlers.ClientHandler;
import docotel.laporanta.R;

public class ClientActivity extends ListActivity implements GetListListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        setActivityToolbar((Toolbar) findViewById(R.id.ClientToolbar), "Client List");
        setListHandler(new ClientHandler(this, this, Request.Method.GET,
                APIHandler.getClientsURL(), null, null));
        setListView((ListView) findViewById(R.id.ClientListView), new ClientAdapter(this, list), null, null);
    }
}
