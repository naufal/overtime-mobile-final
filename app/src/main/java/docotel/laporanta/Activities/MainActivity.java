package docotel.laporanta.Activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import docotel.laporanta.Handlers.ApplicationHelper;
import docotel.laporanta.R;

public class MainActivity extends AppCompatActivity {
    private ImageView imgViewDepartment;
    private ImageView imgViewClient;
    private ImageView imgViewCompany;
    private ImageView imgViewEmployee;
    private ImageView imgViewProject;
    private ImageView imgViewOvertime;
    private Toolbar MainToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainToolbar = (Toolbar) findViewById(R.id.MainToolbar);
        MainToolbar.setTitle("Docotel Overtime");
        setSupportActionBar(MainToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();

        imgViewDepartment = (ImageView) findViewById(R.id.imgViewDepartment);
        imgViewDepartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, DepartmentActivity.class));
            }
        });

        imgViewClient = (ImageView) findViewById(R.id.imgViewClient);
        imgViewClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ClientActivity.class));
            }
        });

        imgViewCompany = (ImageView) findViewById(R.id.imgViewCompany);
        imgViewCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CompanyActivity.class));
            }
        });

        imgViewEmployee = (ImageView) findViewById(R.id.imgViewEmployee);
        imgViewEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, EmployeeActivity.class));
            }
        });

        imgViewProject = (ImageView) findViewById(R.id.imgViewProject);
        imgViewProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ProjectActivity.class));
            }
        });

        imgViewOvertime = (ImageView) findViewById(R.id.imgViewOvertime);
        imgViewOvertime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, OvertimeActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                ApplicationHelper.setToken(this, "");
                startActivity(new Intent(this, SplashScreen.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);
        return true;
    }
}
