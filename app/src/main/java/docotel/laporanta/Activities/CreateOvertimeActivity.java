package docotel.laporanta.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docotel.laporanta.Handlers.AlertDialogHelper;
import docotel.laporanta.Adapters.ProjectAdapter;
import docotel.laporanta.Handlers.ApplicationHelper;
import docotel.laporanta.Handlers.APIHandler;
import docotel.laporanta.Handlers.CreateOvertimeHandler;
import docotel.laporanta.Handlers.RequestHandler;
import docotel.laporanta.Listeners.CreateOvertimeListener;
import docotel.laporanta.Models.EmployeeModel;
import docotel.laporanta.Models.ProjectModel;
import docotel.laporanta.R;

public class CreateOvertimeActivity extends AppCompatActivity implements CreateOvertimeListener {
    private EditText otc_Date, otc_ClockIn, otc_ClockOut;
    private Toolbar toolbar;
    private Calendar calendar = Calendar.getInstance();
    private Spinner ProjectListSpinner;
    private List<ProjectModel> Projects = new ArrayList<>();
    private ProjectAdapter AdapterProject;
    private CreateOvertimeHandler CreateOvertimeHelper = new CreateOvertimeHandler(this);
    private SharedPreferences Preferences;
    private String token = "";
    private ProgressDialog progressDialog;
    private boolean isLoadingProject = false;
    private CheckBox otc_HolidayCheckBox;

    private int overtime_type_id;
    private int project_id;
    private String date;
    private String clock_in;
    private String clock_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_overtime);

        toolbar = (Toolbar) findViewById(R.id.CreateOvertimeToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Overtime");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Preferences = getSharedPreferences("OvertimeDocotel", MODE_PRIVATE);
        token = Preferences.getString("token", "");

        otc_HolidayCheckBox = (CheckBox) findViewById(R.id.otc_HolidayCheckBox);

        otc_Date = (EditText) findViewById(R.id.otc_Date);
        otc_Date.setOnClickListener(new View.OnClickListener() {
            SimpleDateFormat toDateFormatter = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat toSubmitFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat toStringFormatter = new SimpleDateFormat("EEEE, dd MMMM yyyy");
            @Override
            public void onClick(View view) {
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(CreateOvertimeActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedYear, int selectedMonth, int selectedDay) {
                        selectedMonth = selectedMonth + 1;
//                        otc_Date.setText(selectedDay + " " + selectedMonth + " " + selectedYear);
                        try {
                            Date SelectedDate = toDateFormatter.parse(selectedDay + "-" + selectedMonth + "-" + selectedYear);
                            date = toSubmitFormatter.format(SelectedDate);
                            calendar.setTime(SelectedDate);
                            int DayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                            if (DayOfWeek == 1 || DayOfWeek == 7) overtime_type_id = 1;
                            else overtime_type_id = 2;
                            otc_Date.setText(toStringFormatter.format(SelectedDate));
                        } catch (ParseException e) {
                            AlertDialogHelper.showAlertDialog(CreateOvertimeActivity.this, "Error", e.getMessage(), "Close", null);
                        }
                    }
                }, year, month, day);
                mDatePicker.show();
            }
        });

        otc_ClockIn = (EditText) findViewById(R.id.otc_ClockIn);
        otc_ClockIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker2;
                mTimePicker2 = new TimePickerDialog(CreateOvertimeActivity.this, new TimePickerDialog.OnTimeSetListener(){
                    @Override
                    public void onTimeSet(TimePicker timepicker, int selectedHour, int selectedMinute) {
                        String hour = String.format("%02d", selectedHour);
                        String minute = String.format("%02d", selectedMinute);
                        otc_ClockIn.setText( hour + ":" + minute );
                        clock_in = hour + ":" + minute + ":00";
                    }
                }, hour, minute, true);
                mTimePicker2.show();
            }
        });

        otc_ClockOut = (EditText) findViewById(R.id.otc_ClockOut);
        otc_ClockOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker2;
                mTimePicker2 = new TimePickerDialog(CreateOvertimeActivity.this, new TimePickerDialog.OnTimeSetListener(){
                    @Override
                    public void onTimeSet(TimePicker timepicker, int selectedHour, int selectedMinute) {
                        String hour = String.format("%02d", selectedHour);
                        String minute = String.format("%02d", selectedMinute);
                        otc_ClockOut.setText( hour + ":" + minute );
                        clock_out = hour + ":" + minute + ":00";
                    }
                }, hour, minute, true);
                mTimePicker2.show();
            }
        });

        ProjectListSpinner = (Spinner) findViewById(R.id.otc_ProjectName);
        AdapterProject = new ProjectAdapter(this, Projects);
        ProjectListSpinner.setAdapter(AdapterProject);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.CreateNewOvertimeFAB);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressDialog("Loading...");
                project_id = ((ProjectModel) ProjectListSpinner.getSelectedItem()).getId();
                if (otc_HolidayCheckBox.isChecked()) overtime_type_id = 3;
                StringRequest CreateOvertimeRequest = new StringRequest(Request.Method.POST, APIHandler.getOvertimeURL(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                hideProgressDialog();
                                finish();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                AlertDialogHelper.showAlertDialog(CreateOvertimeActivity.this, "Error", error.getMessage(), "OK", null);
                            }
                        }
                ) {
                    @Override
                    public Map<String,String> getParams() {
                        Map<String,String> Parameters = new HashMap<>();
                        Parameters.put("overtime_type_id", String.valueOf(overtime_type_id));
                        Parameters.put("project_id", String.valueOf(project_id));
                        Parameters.put("date",date);
                        Parameters.put("clock_in", clock_in);
                        Parameters.put("clock_out",clock_out);
                        return Parameters;
                    }
                    @Override
                    public Map<String,String> getHeaders() {
                        Map<String,String> Headers = new HashMap<>();
                        Headers.put("Authorization", "Bearer " + ApplicationHelper.getToken(CreateOvertimeActivity.this));
                        return Headers;
                    };
                };
                new RequestHandler().addToRequestQueue(getApplicationContext(), CreateOvertimeRequest);
            }
        });

        GetData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onSuccessFetchEmployeeList(List<EmployeeModel> EmployeeList) {
        //
    }

    @Override
    public void onSuccessFetchProjectList(List<ProjectModel> ProjectList) {
        this.Projects.addAll(ProjectList);
        AdapterProject.notifyDataSetChanged();
        isLoadingProject = false;
        hideProgressDialog();
    }

    @Override
    public void onFailFetchEmployeeList(String message) {
        hideProgressDialog();
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(message)
                .show();
    }

    private void GetData() {
        // GetEmployeeHandler.GetEmployees(this, token, 1);
        // CreateOvertimeHelper.GetEmployees(this, token);
        CreateOvertimeHelper.GetProjects(this, token);
        isLoadingProject = true;
        showProgressDialog("Loading...");
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
        }
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if ((progressDialog != null)
                && !isLoadingProject) {
            progressDialog.dismiss();
        }
    }
}
