package docotel.laporanta.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;

import docotel.laporanta.Adapters.EmployeeAdapter;
import docotel.laporanta.Listeners.GetListListener;
import docotel.laporanta.Handlers.APIHandler;
import docotel.laporanta.Handlers.EmployeeHandler;
import docotel.laporanta.Models.EmployeeModel;
import docotel.laporanta.R;

public class EmployeeActivity extends ListActivity implements GetListListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);
        setActivityToolbar((Toolbar) findViewById(R.id.EmployeeToolbar), "Employee List");
        setListHandler(new EmployeeHandler(this, this, Request.Method.GET,
                APIHandler.getEmployeesURL(), null, null));
        setListView((ListView) findViewById(R.id.EmployeeListView), new EmployeeAdapter(this, list), null,
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        EmployeeModel Employee = (EmployeeModel) listView.getItemAtPosition(i);
                        Intent intent = new Intent(EmployeeActivity.this, EmployeeDetailActivity.class);
                        intent.putExtra("EmployeeID", Employee.getId());
                        startActivity(intent);
                    }
                });
    }
}
