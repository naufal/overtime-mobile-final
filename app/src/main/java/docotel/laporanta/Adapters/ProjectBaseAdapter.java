package docotel.laporanta.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public abstract class ProjectBaseAdapter extends BaseAdapter {
    protected LayoutInflater inflater;
    protected List<?> list;

    public ProjectBaseAdapter(Activity activity, List<?> list) {
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
