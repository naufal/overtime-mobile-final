package docotel.laporanta.Adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import docotel.laporanta.Models.DepartmentModel;
import docotel.laporanta.R;

/**
 * Created by VanZa on 8/22/2016.
 * Modified by hanakoizumi on 8/26/2016.
 */
public class DepartmentAdapter extends ProjectBaseAdapter {

    public DepartmentAdapter(Activity activity, List<DepartmentModel> list) {
        super(activity, list);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) view = inflater.inflate(R.layout.list_row_department, null);
        DepartmentModel Department = (DepartmentModel) list.get(i);
        ((TextView) view.findViewById(R.id.tv_list_department)).setText(Department.getDepartmentName());
        return view;
    }
}