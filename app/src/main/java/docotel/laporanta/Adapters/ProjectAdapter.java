package docotel.laporanta.Adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import docotel.laporanta.Models.ProjectModel;
import docotel.laporanta.R;

/**
 * Created by VanZa on 8/23/2016.
 * Modified by hanakoizumi on 8/26/2016.
 */
public class ProjectAdapter extends ProjectBaseAdapter {

    public ProjectAdapter(Activity activity, List<ProjectModel> list) {
        super(activity, list);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) view = inflater.inflate(R.layout.list_row_project, null);
        ProjectModel Project = (ProjectModel) list.get(i);
        ((TextView) view.findViewById(R.id.tv_project_name)).setText(Project.getProjectName());
        return view;
    }
}
