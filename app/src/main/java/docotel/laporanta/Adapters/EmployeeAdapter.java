package docotel.laporanta.Adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import docotel.laporanta.Models.EmployeeModel;
import docotel.laporanta.R;

/**
 * Created by VanZa on 8/23/2016.
 * Modified by hanakoizumi on 8/26/2016.
 */
public class EmployeeAdapter extends ProjectBaseAdapter {

    public EmployeeAdapter(Activity activity, List<EmployeeModel> list) {
        super(activity, list);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) view = inflater.inflate(R.layout.list_row_employee, null);
        EmployeeModel Employee = (EmployeeModel) list.get(i);
        ((TextView) view.findViewById(R.id.tv_employee_name)).setText(Employee.getEmployeeName());
        ((TextView) view.findViewById(R.id.tv_employee_position)).setText(Employee.getPosition());
        return view;
    }
}
