package docotel.laporanta.Adapters;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import docotel.laporanta.Models.OvertimeModel;
import docotel.laporanta.R;

/**
 * Created by hanakoizumi on 25/08/16.
 */
public class OvertimeAdapter extends ProjectBaseAdapter {

    public OvertimeAdapter(Activity activity, List<OvertimeModel> list) {
        super(activity, list);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SimpleDateFormat DateFormatter = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat TimeFormatter = new SimpleDateFormat("hh:mm");

        if (view == null) view = inflater.inflate(R.layout.list_row_overtime, null);
        OvertimeModel Overtime = (OvertimeModel) list.get(i);

        String description = "<b>" +
                Overtime.getEmployee().getEmployeeName() +
                "</b> pada tanggal <b>" +
                DateFormatter.format(Overtime.getCreatedAt()) +
                "</b> mengajukan lembur untuk tanggal <b>" +
                DateFormatter.format(Overtime.getDate()) +
                "</b> pukul <b>" +
                TimeFormatter.format(Overtime.getClockIn()) +
                "</b> hingga <b>" +
                TimeFormatter.format(Overtime.getClockOut()) +
                "</b> dalam rangka proyek <b>" +
                Overtime.getProject().getProjectName() + "</b>.";

        String approval = "<b>" +
                Overtime.getCreatedBy() +
                "</b> pada <b>" +
                DateFormatter.format(Overtime.getUpdatedAt());

        ((TextView) view.findViewById(R.id.otd_OvertimeDetail)).setText(Html.fromHtml(description));

        BootstrapLabel status_label = (BootstrapLabel) view.findViewById(R.id.blabel_status);
        switch (Overtime.getOvertimeStatus()) {
            case WAITING:
                status_label.setBootstrapBrand(DefaultBootstrapBrand.WARNING);
                status_label.setText("Menunggu Persetujuan");
                approval = "";
                break;
            case APPROVED:
                status_label.setBootstrapBrand(DefaultBootstrapBrand.SUCCESS);
                status_label.setText("Disetujui");
                break;
            case REJECTED:
                status_label.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
                status_label.setText("Ditolak");
                break;
            default:
                break;
        }

        ((TextView) view.findViewById(R.id.otd_ModifiedBy)).setText(Html.fromHtml(approval));

        return view;
    }
}
