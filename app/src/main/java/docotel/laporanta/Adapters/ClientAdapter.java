package docotel.laporanta.Adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import docotel.laporanta.Models.ClientModel;
import docotel.laporanta.R;

/**
 * Created by VanZa on 8/22/2016.
 * Modified by hanakoizumi on 8/26/2016.
 */
public class ClientAdapter extends ProjectBaseAdapter {

    public ClientAdapter(Activity activity, List<ClientModel> ClientList) {
        super(activity, ClientList);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) view = inflater.inflate(R.layout.list_row_client, null);
        ClientModel Client = (ClientModel) list.get(i);
        ((TextView) view.findViewById(R.id.tv_client_name)).setText(Client.getClientName());
        ((TextView) view.findViewById(R.id.tv_client_prefix)).setText(Client.getPrefix());
        return view;
    }
}
