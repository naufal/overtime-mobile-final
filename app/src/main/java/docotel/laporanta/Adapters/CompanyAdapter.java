package docotel.laporanta.Adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import docotel.laporanta.Models.CompanyModel;
import docotel.laporanta.R;

/**
 * Created by VanZa on 8/22/2016.
 * Modified by hanakoizumi on 8/26/2016.
 */
public class CompanyAdapter extends ProjectBaseAdapter {

    public CompanyAdapter(Activity activity, List<CompanyModel> list) {
        super(activity, list);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) view = inflater.inflate(R.layout.list_row_company, null);
        CompanyModel Company = (CompanyModel) list.get(i);
        ((TextView) view.findViewById(R.id.tv_company_name)).setText(Company.getCompanyName());
        ((TextView) view.findViewById(R.id.tv_company_prefix)).setText(Company.getPrefix());
        return view;
    }
}
